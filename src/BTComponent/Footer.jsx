import React from "react";
import style from "../asset/style.module.css";

const Footer = () => {
  return <div className={style.footer}>Copyright © Your Website 2023</div>;
};

export default Footer;
