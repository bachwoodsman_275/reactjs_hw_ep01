import React from "react";
import Banner from "./Banner";
import Item from "./Item";

const Body = () => {
  return (
    <div>
      <Banner></Banner>
      <Item></Item>
    </div>
  );
};

export default Body;
